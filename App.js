/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  StatusBar,
  Platform
} from 'react-native';
import AppContainer from './src/Navigators/StackNavigators/AppNavigator'

// console.disableYellowBox = true;
class App extends Component {

  render() {
    return (
      <AppContainer />
    )
  }
};

const styles = StyleSheet.create({

});

export default App;
