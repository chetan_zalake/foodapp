# foodapp

Steps to run the project:

1. Clone the repository.
2. npm install

To run the app on Android

1. react-native run-android

To run the app on iOS

1. cd ios/
2. pod install
3. cd ..
4. react-native run-ios

