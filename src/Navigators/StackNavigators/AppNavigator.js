import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import TabBar from '../TabBarNavigator/TabBar'

const AppNavigator = createStackNavigator(
  {
    TabBar: { screen: TabBar },
  },
  {
    initialRouteName: 'TabBar',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const AppStackNavigator = createAppContainer(AppNavigator);

export default AppStackNavigator