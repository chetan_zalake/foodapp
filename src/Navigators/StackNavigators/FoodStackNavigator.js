import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Food from '../../Screens/TabBarScreens/FoodStackScreens/Food/Food'
import FoodDetails from '../../Screens/TabBarScreens/FoodStackScreens/FoodDetails/FoodDetails'
import FoodRecipie from  '../../Screens/TabBarScreens/FoodStackScreens/FoodRecipie/FoodRecipie'

const FoodNavigator = createStackNavigator(
  {
    Food: { screen: Food },
    FoodDetails: {screen: FoodDetails},
    FoodRecipie: {screen: FoodRecipie}
  },
  {
    initialRouteName: 'Food',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const FoodStackNavigator = createAppContainer(FoodNavigator);

export default FoodStackNavigator