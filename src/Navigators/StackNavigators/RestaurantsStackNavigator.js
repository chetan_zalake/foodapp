import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Restaurants from '../../Screens/TabBarScreens/RestaurantStackScreens/Restaurants/Restaurants'
import RestaurantDetails from '../../Screens/TabBarScreens/RestaurantStackScreens/RestaurantDetails/RestaurantDetails'
const RestaurantsNavigator = createStackNavigator(
  {
    Restaurants: { screen: Restaurants },
    RestaurantDetails: {screen: RestaurantDetails}
  },
  {
    initialRouteName: 'Restaurants',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const RestaurantsStackNavigator = createAppContainer(RestaurantsNavigator);

export default RestaurantsStackNavigator