import {  createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import React, { Component } from 'react';
import { Image, Platform } from 'react-native';
import FoodStack from '../StackNavigators/FoodStackNavigator'
import RestaurantsStack from '../StackNavigators/RestaurantsStackNavigator'
import { textFontSize } from '../../constants/appConstants';
import { SF_PRO_REGULAR } from '../../constants/fonts';
import { APP_GREEN, INACTIVE_TINT_COLOR, WHITE_COLOR } from '../../constants/colors';

const TabNavigator = createBottomTabNavigator({
    Food: {
        screen: FoodStack,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor }) => <Image resizeMode='contain' style={{ width: 20, height: 20, tintColor: tintColor}} source={require('../../../assets/images/icons-food/icons-food.png')} />
        })

    },
    Restaurants: {
        screen: RestaurantsStack,
        navigationOptions: ({ navigation }) => ({
            
            tabBarIcon: ({ tintColor }) => <Image resizeMode='contain' style={{ width: 20, height: 20, tintColor: tintColor }} source={require('../../../assets/images/icons-restaurant/icons-restaurant.png')} />
        })
    }
},
    {
        tabBarOptions: {
            activeTintColor: APP_GREEN,
            inactiveTintColor: INACTIVE_TINT_COLOR,
            allowFontScaling: false,
            showLabel: false,
            style: {
                backgroundColor: WHITE_COLOR,
                height: 58
            },
            tabStyle: {
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'
            }
        },
    },
    (Platform.OS === 'android')
        ? {
            tabBarComponent: props => <TabBarComponent {...props} />,
            tabBarPosition: 'bottom'
        }
        : {
            // don't change tabBarComponent here - it works on iOS after all.
        });

export default createAppContainer(TabNavigator);