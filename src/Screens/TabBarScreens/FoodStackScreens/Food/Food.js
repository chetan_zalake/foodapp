import React, { Component } from 'react';
import { View } from 'react-native';
import AppHeader from '../../../../reuseable/header/AppHeader';
import FoodListView from './FoodListView';

export default class Food extends Component {

  foodOptionSelected = (index, title) => {
    this.props.navigation.navigate('FoodDetails', { title:  title})
  }

  render() {
    return (
      <View style = {{flex: 1}}>
        <AppHeader showRightIcon = {true} rightIconImage = {require('../../../../../assets/images/icons-search/icons-search.png')}
          rightNavigationButtonAction = {() => {

          }}
          title = {'Recipies'}
        />
        <FoodListView foodOptionSelected = {(index, title) => this.foodOptionSelected(index, title)}/>
      </View>
    );
  }
}
