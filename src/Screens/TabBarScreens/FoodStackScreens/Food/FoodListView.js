import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import FoodListViewCell from './FoodListViewCell'
import { LogData } from '../../../../Utils/LogUtils';

const data = [
    {
        title: 'START YOUR DAY RIGHT',
        subTitle: 'Breakfast'
    },
    {
        title: 'POWER VEGETABLES',
        subTitle: 'Vegetarian'
    }
]

export default class FoodListView extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={data}
                    style={{ flex: 1 }}
                    renderItem=
                    {({ item, index }) => {
                        LogData(item, index)
                        return (
                            <FoodListViewCell item = {item} index = {index} foodOptionSelected = {(index, title) => this.props.foodOptionSelected(index, title)}/>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
    </View>
        );
    }
}
