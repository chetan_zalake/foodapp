import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { SF_PRO_REGULAR, SF_PRO_MEDIUM } from '../../../../constants/fonts';
import { WHITE_COLOR } from '../../../../constants/colors';
import LinearGradient from 'react-native-linear-gradient';

export default class FoodListViewCell extends PureComponent {
    render() {
        return (
                <TouchableOpacity onPress = {() => this.props.foodOptionSelected(this.props.index, this.props.item.subTitle)} style = {{marginHorizontal: 16, marginTop: 16, borderRadius: 8, height: 280, overflow: 'hidden'}}>
                    <Image style={{ width: '100%', height: '100%'}} source={require('../../../../../assets/images/rest_1.jpg')} />
                    <LinearGradient colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0)']} style={{width: '100%', height: 120, position: 'absolute'}}>
                        <View style = {{position: 'absolute', marginHorizontal: 20, marginTop: 15}}>
                            <Text allowFontScaling={false} style = {{fontSize: 15, fontFamily: SF_PRO_REGULAR, color: 'rgba(255,255,255,0.7)', }}>{this.props.item.title}</Text>
                            <Text allowFontScaling={false} style = {{fontSize: 28, fontFamily: SF_PRO_MEDIUM, color: WHITE_COLOR, marginTop: 5}}>{this.props.item.subTitle}</Text>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>
        );
    }
}
