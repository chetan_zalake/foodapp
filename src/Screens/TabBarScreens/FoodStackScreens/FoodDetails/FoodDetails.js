import React, { Component } from 'react';
import { View } from 'react-native';
import FoodDetailsListView from './FoodDetailsListView';
import BasicAppHeader from '../../../../reuseable/header/BasicAppHeader';

export default class FoodDetails extends Component {

  foodOptionSelected = (index, title) => {
    this.props.navigation.navigate('FoodRecipie', { title:  title, backTitle: this.props.navigation.state.params.title})
  }

  render() {
    return (
      <View style = {{flex: 1}}>
        <BasicAppHeader 
            showRightIcon = {true} 
            rightIconImage = {require('../../../../../assets/images/icons-search/icons-search.png')}
            rightNavigationButtonAction = {() => {
                
            }}

            showLeftIcon = {true}
            leftButtonTitle = {this.props.navigation.state.params.title}
            title = {this.props.navigation.state.params.title}
            onLeftButtonPressed = {() => {
                this.props.navigation.goBack()
            }}
        />
        <FoodDetailsListView foodOptionSelected = {(index, title) => this.foodOptionSelected(index, title)}/>
      </View>
    );
  }
}
