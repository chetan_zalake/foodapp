import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import FoodDetailsListViewCell from './FoodDetailsListViewCell'
import { LogData } from '../../../../Utils/LogUtils';

const data = [
    {
        title: 'Morning Smoothies',
        details: '2 people · 10 minutes'
    },
    {
        title: 'Fruity Oatmeal',
        details: '2 people · 15 minutes'
    },
    {
        title: 'Belgian Waffles',
        details: '2 people · 45 minutes'
    },
    {
        title: 'French Toast',
        details: '2 people · 10 minutes'
    },
    {
        title: 'Morning Smoothies',
        details: '2 people · 10 minutes'
    },
    {
        title: 'Morning Smoothies',
        details: '2 people · 10 minutes'
    },
    {
        title: 'Morning Smoothies',
        details: '2 people · 10 minutes'
    }
]

export default class FoodDetailsListView extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={data}
                    style={{ flex: 1 }}
                    renderItem=
                    {({ item, index }) => {
                        LogData(item, index)
                        return (
                            <FoodDetailsListViewCell item = {item} index = {index} foodOptionSelected = {(index, title) => this.props.foodOptionSelected(index, title)}/>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
    </View>
        );
    }
}
