import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { SF_PRO_REGULAR, SF_PRO_MEDIUM } from '../../../../constants/fonts';
import { WHITE_COLOR } from '../../../../constants/colors';
import LinearGradient from 'react-native-linear-gradient';

export default class FoodDetailsListViewCell extends PureComponent {
    render() {
        return (
            <TouchableOpacity onPress = {() => this.props.foodOptionSelected(this.props.index, this.props.item.title)} style={{ marginHorizontal: 16, marginTop: 16, borderRadius: 8, height: 160, overflow: 'hidden' }}>
                <Image style={{ width: '100%', height: '100%' }} source={require('../../../../../assets/images/rest_1.jpg')} />
                <View style={{ position: 'absolute', width: '100%', height: '100%' }}>
                    <LinearGradient colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0)']} style={{ width: '100%', height: 120 }}>
                        <Text allowFontScaling={false} style={{ fontSize: 28, marginHorizontal: 15, fontFamily: SF_PRO_MEDIUM, color: WHITE_COLOR, marginTop: 15 }}>{this.props.item.title}</Text>
                    </LinearGradient>
                    <Text allowFontScaling={false} style={{ fontSize: 16, marginHorizontal: 15, fontFamily: SF_PRO_REGULAR, color: WHITE_COLOR, marginTop: 5 }}>{this.props.item.details}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}
