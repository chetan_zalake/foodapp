
import React, { Component } from 'react';
import { View, StyleSheet, Platform, TouchableOpacity, Text, Modal } from 'react-native';
import ImagesHeader from '../../../../reuseable/header/ImagesHeader';
import IconWithTitleView from '../../../../reuseable/IconWithTitleView/IconWithTitleView';
import { WHITE_COLOR, APP_GREEN, VIEW_BACKGROUND_COLOR } from '../../../../constants/colors';
import { SF_PRO_MEDIUM, SF_PRO_REGULAR } from '../../../../constants/fonts';
import RecipieListView from './RecipieListView';
import IngredientsListView from './Ingredients/IngredientsListView';

export default class Food extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            data: [
                {
                    title: 'Flour',
                    subTitle: '2 cups',
                    isSelected: false
                },
                {
                    title: 'Baking powder',
                    subTitle: '4 teaspoons',
                    isSelected: false
                },
                {
                    title: 'Salt',
                    subTitle: '½ teaspoon',
                    isSelected: false
                },
                {
                    title: 'Sugar',
                    subTitle: '¼ cup',
                    isSelected: false
                },
                {
                    title: 'Egg',
                    subTitle: '2',
                    isSelected: false
                },
                {
                    title: 'Milk',
                    subTitle: '2 cups',
                    isSelected: false
                },
                {
                    title: 'Vegetable oil',
                    subTitle: '½ cup',
                    isSelected: false
                },
            ]
        };
    }

    seeIngredientsButtonAction = () => {
        this.setState({ modalVisible: true })
    }

    itemSelected = (index) => {
        const newDataArray = [...this.state.data];
        newDataArray[index].isSelected = !this.state.data[index].isSelected;
        this.setState({ data: newDataArray });
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: VIEW_BACKGROUND_COLOR }}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false })
                    }}>
                    <IngredientsListView downArrowButtonAction={() => this.setState({ modalVisible: false })} data={this.state.data} itemSelected={(index) => this.itemSelected(index)} />
                </Modal>
                <ImagesHeader showRightIcon={true} rightIconImage={require('../../../../../assets/images/icons-bookmark/icons-bookmark.png')}
                    rightNavigationButtonAction={() => {

                    }}
                    showLeftIcon={true}
                    leftButtonTitle={this.props.navigation.state.params.backTitle}
                    title={this.props.navigation.state.params.title}
                    onLeftButtonPressed={() => {
                        this.props.navigation.goBack()
                    }}
                    images={[require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'),]}
                />
                <View style={styles.container}>
                    <IconWithTitleView image={require('../../../../../assets/images/icons-restaurant/icons-restaurant.png')} title='6 People' />
                    <IconWithTitleView image={require('../../../../../assets/images/icons-time/icons-time.png')} title='45 minutes' />
                </View>
                <TouchableOpacity style={styles.button} onPress={() => this.seeIngredientsButtonAction()}>
                    <Text allowFontScaling={false} style={styles.buttonText}>
                        See ingredients
                    </Text>
                </TouchableOpacity>
                <RecipieListView />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%', alignItems: 'center', flexDirection: "row", justifyContent: 'space-evenly', backgroundColor: WHITE_COLOR,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowRadius: 5,
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 5
            }
        }),
    },
    button: {
        marginTop: 16,
        marginHorizontal: 16,
        backgroundColor: APP_GREEN,
        justifyContent: 'center',
        borderRadius: 8,
        height: 44,
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 17,
        fontFamily: SF_PRO_REGULAR
    }
})