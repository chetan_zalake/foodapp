import React, { Component } from 'react';
import { FlatList, View, StyleSheet, Dimensions, TouchableOpacity, Text, Image } from 'react-native';
import IngredientsListViewCell from './IngredientsListViewCell'
import SafeAreaView from 'react-native-safe-area-view';
import { WHITE_COLOR, APP_GREEN, VIEW_BACKGROUND_COLOR } from '../../../../../constants/colors';
import { SF_PRO_REGULAR } from '../../../../../constants/fonts';
const deviceHeight = Dimensions.get("window").height;

export default class IngredientsListView extends Component {

    render_FlatList_header = () => {
        return (
            <View style = {{width: '100%', height: 44}}>
                <View style = {{width: '100%', height: '100%' , alignItems: 'center', justifyContent: 'center' ,position: 'absolute'}}>
                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN }}>Ingredients</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{marginHorizontal: 15 ,width: 40, height: '100%', }}>
                    <Image resizeMode={'contain'} style={{ flex: 1, tintColor: APP_GREEN }} source={require('../../../../../../assets/images/icons-arrow-down/icons-arrow-down.png')} />
                </TouchableOpacity>
                <View style={styles.separator} />
            </View>
        )

    };

    render_FlatList_footer = () => {
        return (
            <View style = {{width: '100%', height: 60, marginTop: 15, marginBottom: 15}}>
                <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{marginHorizontal: 15 , height: 44, borderRadius: 8, backgroundColor: APP_GREEN, alignItems: 'center', justifyContent: 'center' }}>
                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: WHITE_COLOR }}>Add to Reminders</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, marginTop: -50, backgroundColor: 'rgba(0,0,0,0.6)' }}>
                    <View style={{ width: '100%', backgroundColor: WHITE_COLOR, position: 'absolute', bottom: 0 }}>
                        <SafeAreaView >
                            <FlatList
                                data={this.props.data}
                                ListHeaderComponent={this.render_FlatList_header}
                                ListFooterComponent={this.render_FlatList_footer}
                                style={styles.tableStyle}
                                renderItem=
                                {({ item, index }) => {
                                    return (
                                        <IngredientsListViewCell item={item} index={index} itemSelected = {(index) => this.props.itemSelected(index)}/>
                                    )
                                }}
                                keyExtractor={(item, index) => index.toString()}
                                stickyHeaderIndices={[0]}
                            />
                        </SafeAreaView>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    tableStyle: {
        borderTopLeftRadius: 8, borderTopRightRadius: 8, width: '100%', maxHeight: deviceHeight * .7, position: 'absolute', bottom: 0, backgroundColor: WHITE_COLOR

    },
    separator: { backgroundColor: VIEW_BACKGROUND_COLOR, height: 1, width: '100%' }
})
