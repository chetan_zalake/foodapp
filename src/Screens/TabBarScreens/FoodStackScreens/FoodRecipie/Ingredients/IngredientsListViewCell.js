import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { SF_PRO_REGULAR, SF_PRO_MEDIUM } from '../../../../../constants/fonts';
import { GRAY_COLOR, VIEW_BACKGROUND_COLOR, WHITE_COLOR, APP_GREEN } from '../../../../../constants/colors';
import { LogData } from '../../../../../Utils/LogUtils';

export default class IngredientsListViewCell extends PureComponent {
    render() {
        LogData('this.props.item.isSelected:', this.props.item.isSelected)
        return (
            <View style = {{opacity: this.props.item.isSelected ? 0.3 : 1 ,}}>
            <TouchableOpacity onPress = {() => this.props.itemSelected(this.props.index)} style={{width: '100%',backgroundColor: WHITE_COLOR, borderTopLeftRadius: this.props.index === 0 ? 8 : 0, borderBottomLeftRadius: this.props.index === 3 ? 8 : 0, borderTopRightRadius: this.props.index === 0 ? 8 : 0, borderBottomRightRadius: this.props.index === 3 ? 8 : 0 , flexDirection: 'row', }}>
                <Image style={{ width: 22, height: 22,tintColor: APP_GREEN, marginLeft: 15, alignSelf: 'center' }} source={this.props.item.isSelected ? require('../../../../../../assets/images/icons-circle/icons-circle.png') : require('../../../../../../assets/images/icons-circle-empty/icons-circle-empty.png')} />
                <View style={{ marginHorizontal: 10}}>
                    <Text allowFontScaling={false} style={{ fontSize: 17, marginTop: 5,fontFamily: SF_PRO_REGULAR, color: 'black' }}>{this.props.item.title}</Text>
                    <Text allowFontScaling={false} style={{ fontSize: 15, marginRight: 15, marginVertical: 5 ,fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR }}>{this.props.item.subTitle}</Text>
                </View>
                <View style={styles.separator} />
            </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    separator: { backgroundColor: VIEW_BACKGROUND_COLOR, height: 1, width: '100%', position: 'absolute', bottom: 0 }
});
