import React, { Component } from 'react';
import { FlatList, View, StyleSheet } from 'react-native';
import RecipieListViewCell from './RecipieListViewCell'
import { LogData } from '../../../../Utils/LogUtils';
import { WHITE_COLOR } from '../../../../constants/colors';

const data = [
    {
        title: 'Oil the waffle maker.',
    },
    {
        title: 'Sift the dry ingredients together in a large bowl.',
    },
    {
        title: 'In separate bowl, separate egg whites and beat until stiff peaks form.',
    },
    {
        title: 'Mix together the egg yolks, milk, oil, and vanilla, stir slightly.',
    }
]

export default class RecipieListView extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={data}
                    style={styles.tableStyle}
                    renderItem=
                    {({ item, index }) => {
                        return (
                            <RecipieListViewCell item={item} index={index} />
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tableStyle: {
        marginHorizontal: 15, marginVertical: 15, borderRadius: 8,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.8)', 
                shadowRadius: 5, 
                shadowOpacity: 0.2,
            },
            android: {
              elevation: 5
            }
          }),
    }
})
