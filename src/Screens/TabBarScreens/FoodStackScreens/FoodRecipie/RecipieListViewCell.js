import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { SF_PRO_REGULAR } from '../../../../constants/fonts';
import { GRAY_COLOR, VIEW_BACKGROUND_COLOR, WHITE_COLOR } from '../../../../constants/colors';

export default class RecipieListViewCell extends PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: WHITE_COLOR, borderTopLeftRadius: this.props.index === 0 ? 8 : 0, borderBottomLeftRadius: this.props.index === 3 ? 8 : 0, borderTopRightRadius: this.props.index === 0 ? 8 : 0, borderBottomRightRadius: this.props.index === 3 ? 8 : 0 }}>
                <View style={{ marginHorizontal: 15, flexDirection: 'row' }}>
                    <Text allowFontScaling={false} style={{ fontSize: 17, padding: 10, fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR }}>{this.props.index + 1}</Text>
                    <Text allowFontScaling={false} style={{ fontSize: 17, padding: 10, marginRight: 15, fontFamily: SF_PRO_REGULAR, color: 'black' }}>{this.props.item.title}</Text>
                </View>
                <View style={styles.separator} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    separator: { backgroundColor: VIEW_BACKGROUND_COLOR, height: 1, width: '100%' }
});
