import React, { Component } from 'react';
import { FlatList, View, StyleSheet, Dimensions, TouchableOpacity, Text, Image } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { WHITE_COLOR, APP_GREEN, VIEW_BACKGROUND_COLOR, GRAY_COLOR } from '../../../../constants/colors';
import { SF_PRO_REGULAR, SF_PRO_BOLD } from '../../../../constants/fonts';
import { LogData } from '../../../../Utils/LogUtils';
const deviceHeight = Dimensions.get("window").height;

export default class IngredientsListView extends Component {

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.6)' }}>
                <View style={styles.tableStyle}>
                    <SafeAreaView style={{}}>
                        <View style={{ width: '100%', height: 44 }}>
                            <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                                <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN }}>Reservation</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{ marginHorizontal: 15, width: 40, height: '100%', }}>
                                <Image resizeMode={'contain'} style={{ flex: 1, tintColor: APP_GREEN }} source={require('../../../../../assets//images/icons-arrow-down/icons-arrow-down.png')} />
                            </TouchableOpacity>
                            <View style={styles.separator} />
                        </View>
                        <View style={{ width: '100%' }}>
                            <View style={{ flexDirection: 'row', width: '100%', height: 120 }}>
                                <View style={{ padding: 15, flex: 0.70, justifyContent: "center" }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_BOLD, color: 'black' }}>Gusteau’s</Text>
                                    <Text allowFontScaling={false} style={{ fontSize: 15, fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR, marginTop: 5 }}>{"5 Avenue Anatole" + '\n' + "Paris, France"}</Text>
                                </View>
                                <View style={{ flex: 0.30, borderRadius: 8, marginVertical: 15, marginRight: 15, backgroundColor: 'blue', overflow: "hidden" }}>
                                    <Image resizeMode={'repeat'} style={{ width: '100%', height: '100%' }} source={require('../../../../../assets/images/rest_1.jpg')} />
                                </View>
                            </View>

                        </View>
                        <View style={styles.separator} />
                        <View>
                            <View style={{ height: 44, marginTop: 15, marginHorizontal: 15, backgroundColor: 'rgba(115, 199, 0, 0.1)', borderRadius: 8 }}>
                                <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN }}>Today</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{ width: 40, height: '100%', position: 'absolute', right: 15 }}>
                                    <Image resizeMode={'contain'} style={{ flex: 1, tintColor: APP_GREEN }} source={require('../../../../../assets//images/icons-arrow-down/icons-arrow-down.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            <View style={{ height: 44, marginTop: 15, marginHorizontal: 15, backgroundColor: 'rgba(115, 199, 0, 0.1)', borderRadius: 8 }}>
                                <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN }}>2 People</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{ width: 40, height: '100%', position: 'absolute', right: 15 }}>
                                    <Image resizeMode={'contain'} style={{ flex: 1, tintColor: APP_GREEN }} source={require('../../../../../assets//images/icons-plus/icons-plus.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.downArrowButtonAction()} style={{ width: 40, height: '100%', position: 'absolute', left: 15 }}>
                                    <Image resizeMode={'contain'} style={{ flex: 1, tintColor: APP_GREEN }} source={require('../../../../../assets//images/icons-minus/icons-minus.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            <View style={{ height: 44, marginTop: 15, marginHorizontal: 15, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                <View style={{ width: 74, height: '100%', justifyContent: 'center', justifyContent: 'center', borderRadius: 8, backgroundColor: 'rgba(115, 199, 0, 0.1)' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN, textAlign: 'center' }}>19:00</Text>
                                </View>
                                <View style={{ width: 74, height: '100%', justifyContent: 'center', justifyContent: 'center', borderRadius: 8, backgroundColor: 'rgba(115, 199, 0, 0.1)' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN, textAlign: 'center' }}>19:00</Text>
                                </View>
                                <View style={{ width: 74, height: '100%', justifyContent: 'center', justifyContent: 'center', borderRadius: 8, backgroundColor: 'rgba(115, 199, 0, 0.1)' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN, textAlign: 'center' }}>19:00</Text>
                                </View>
                                <View style={{ width: 74, height: '100%', justifyContent: 'center', justifyContent: 'center', borderRadius: 8, backgroundColor: 'rgba(115, 199, 0, 0.1)' }}>
                                    <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: APP_GREEN, textAlign: 'center' }}>19:00</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: 60, marginTop: 20 }}>
                            <TouchableOpacity onPress={() => LogData("Apple Pay")} style={{ marginHorizontal: 15, height: 44, borderRadius: 8, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                                <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_REGULAR, color: WHITE_COLOR }}>Pay</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tableStyle: {
        borderTopLeftRadius: 8, borderTopRightRadius: 8, width: '100%', position: 'absolute', bottom: 0, backgroundColor: WHITE_COLOR, overflow: 'hidden'

    },
    separator: { backgroundColor: VIEW_BACKGROUND_COLOR, height: 1, width: '100%' }
})
