
import React, { Component } from 'react';
import { View, StyleSheet, Platform, TouchableOpacity, Text, Modal, Image, FlatList } from 'react-native';
import ImagesHeader from '../../../../reuseable/header/ImagesHeader';
import IconWithTitleView from '../../../../reuseable/IconWithTitleView/IconWithTitleView';
import { WHITE_COLOR, APP_GREEN, VIEW_BACKGROUND_COLOR } from '../../../../constants/colors';
import { SF_PRO_MEDIUM, SF_PRO_REGULAR, SF_PRO_BOLD } from '../../../../constants/fonts';
import MapView from 'react-native-maps'
import Geolocation from '@react-native-community/geolocation';
import DetailsModal from './DetailsModal'
import IngredientsListView from './DetailsModal';

export default class RestaurantDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: 37.79,
                longitude: -122.401,
                longitudeDelta: 0.0122,
                latitudeDelta: 0.0122

            },
            modalVisible: false,
            data: [
                {
                    title: 'Flour',
                }, {
                    title: 'Flour',
                }, {
                    title: 'Flour',
                }, {
                    title: 'Flour',
                }, {
                    title: 'Flour',
                }, {
                    title: 'Flour',
                }
            ]
        };
    }

    makeReservation = () => {
        this.setState({ modalVisible: true })
    }

    downArrowButtonAction = () => {
        this.setState({ modalVisible: false })
    }

    render() {
        let marker = <MapView.Marker key={'a'} coordinate={{ latitude: this.state.region.latitude + 0.0006, longitude: this.state.region.longitude + 0.0006 }} />
        return (
            <View style={{ flex: 1, backgroundColor: VIEW_BACKGROUND_COLOR }}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false })
                    }}>
                    <DetailsModal downArrowButtonAction = {() => this.downArrowButtonAction()}/>
                </Modal>
                <ImagesHeader showRightIcon={true} rightIconImage={require('../../../../../assets/images/icons-bookmark/icons-bookmark.png')}
                    rightNavigationButtonAction={() => {

                    }}
                    showLeftIcon={true}
                    leftButtonTitle={'Restaurants'}
                    title={this.props.navigation.state.params.restaurant.name}
                    onLeftButtonPressed={() => {
                        this.props.navigation.goBack()
                    }}
                    images={[require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'), require('../../../../../assets/images/rest_1.jpg'),]}
                />
                <View style={styles.container}>
                    <IconWithTitleView image={require('../../../../../assets/images/icons-money/icons-money.png')} title='€€' />
                    <IconWithTitleView isRatings = {true} ratingsCount = {this.props.navigation.state.params.restaurant.ratings} image={require('../../../../../assets/images/icons-circle/icons-circle.png')} title='129 reviews' />
                    <IconWithTitleView image={require('../../../../../assets/images/icons-time/icons-time.png')} title='18:00 - 22:00' />
                </View>
                <MapView
                    showsUserLocation={true}
                    style={styles.map}
                    region={this.state.region}
                >
                    {marker}
                </MapView>
                <TouchableOpacity style={styles.button} onPress={() => this.makeReservation()}>
                    <Text allowFontScaling={false} style={styles.buttonText}>
                        Make Reservation
                    </Text>
                </TouchableOpacity>
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={['The restaurant was degraded to a 4-star rating after feared food critic Anton Ego (possibly deliberately) wrote a scathing review regarding Gusteau’s cooking.', 'The restaurant was degraded to a 4-star rating after feared food critic Anton Ego (possibly deliberately) wrote a scathing review regarding Gusteau’s cooking.', 'The restaurant was degraded to a 4-star rating after feared food critic Anton Ego (possibly deliberately) wrote a scathing review regarding Gusteau’s cooking.', 'The restaurant was degraded to a 4-star rating after feared food critic Anton Ego (possibly deliberately) wrote a scathing review regarding Gusteau’s cooking.']}
                        style={styles.tableStyle}
                        renderItem=
                        {({ item, index }) => {
                            return (
                                <View onPress={() => this.props.restaurantSelected(this.props.index)} style={styles.cellStyle}>
                                    <Text allowFontScaling={false} style={{ padding: 15,fontSize: 17, fontFamily: SF_PRO_REGULAR, color: 'black' }}>{item}</Text>
                                </View>
                            )
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%', alignItems: 'center', flexDirection: "row", justifyContent: 'space-evenly', backgroundColor: WHITE_COLOR,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowRadius: 5,
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 5
            }
        }),
    },
    button: {
        marginHorizontal: 16,
        backgroundColor: APP_GREEN,
        justifyContent: 'center',
        borderRadius: 8,
        height: 44,
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 17,
        fontFamily: SF_PRO_REGULAR
    },
    map: {
        width: '100%',
        height: 200
    },
    cellStyle: {
        borderRadius: 8, marginHorizontal: 15, marginVertical: 7.5, flexDirection: 'row', borderColor: 'black', backgroundColor: WHITE_COLOR,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.8)',
                shadowRadius: 5,
                shadowOpacity: 0.2,
            },
            android: {
                elevation: 2
            }
        }),
    }
})