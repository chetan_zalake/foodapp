import React, { Component } from 'react';
import { Text, View, Dimensions, StyleSheet, Platform } from 'react-native';
import MapView from 'react-native-maps'
import Geolocation from '@react-native-community/geolocation';
import { PermissionsAndroid } from 'react-native';
import AppHeader from '../../../../reuseable/header/AppHeader';
import RestaurantsListView from './RestaurantsListView';
import { restaurantsData } from './RestaurantsData';

// const API_KEY = 'AIzaSyAB3408cK61Xc6Lyfzq4d5wkr5RlfoaM4A'

export const getCurrentLocation = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(position => resolve(position), e => reject(e));
  });
};

export default class Restaurants extends Component {

  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 37.79,
        longitude: -122.401,
        longitudeDelta: Dimensions.get('window').width / Dimensions.get('window').height * 0.0122,
        latitudeDelta: 0.0122

      },
    };
  }

  async requestLocationPermission() {
    if (Platform.OS === 'ios') {
      await Geolocation.requestAuthorization()
      Geolocation.watchPosition(position => {
        if (position) {
          this.getCurrentLocation()
        }

        Geolocation.clearWatch()
      });
      return
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Grant your location access',
          message:
            'App would like to know you location ' +
            'so you can get nearby restaurants',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentLocation()
      } else {
        console.log('permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  getCurrentLocation = () => {
    return getCurrentLocation().then(position => {
      if (position) {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.003,
            longitudeDelta: 0.003,
          },
        });
      }
    },
      err => {
        console.log(err)
      });
  }

  componentDidMount() {

    this.requestLocationPermission()

  }

  restaurantSelected = (index) => {
    this.props.navigation.navigate('RestaurantDetails', { restaurant:  restaurantsData[index]})
  }

  render() {
    let markers = [<MapView.Marker key={'a'} coordinate={{ latitude: this.state.region.latitude + 0.0006, longitude: this.state.region.longitude + 0.0006 }} />, <MapView.Marker key={'b'} coordinate={{ latitude: this.state.region.latitude + 0.0006, longitude: this.state.region.longitude - 0.0006 }} />, <MapView.Marker key={'c'} coordinate={{ latitude: this.state.region.latitude - 0.0006, longitude: this.state.region.longitude - 0.0006 }} />]
    return (
      <View style={styles.container}>
        <AppHeader showRightIcon={true} rightIconImage={require('../../../../../assets/images/icons-search/icons-search.png')}
          rightNavigationButtonAction={() => {

          }}
          title={'Restaurants'}
        />
        <MapView
          showsUserLocation={true}
          style={styles.map}
          region={this.state.region}
        >
          {markers}
        </MapView>
        <RestaurantsListView restaurantSelected = {(index) => this.restaurantSelected(index)}/>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    width: '100%',
    height: 200
  },
});