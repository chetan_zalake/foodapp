export const restaurantsData =
    [{
        name: 'Gusteau’s',
        type: 'French',
        ratings: 2,
        reviews: '128 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },
    {
        name: 'Flo’s V8 Cafe',
        type: 'American',
        ratings: 4,
        reviews: '77 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Pizza Planet',
        type: 'American',
        ratings: 1,
        reviews: '28 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Hayyhausen’s',
        type: 'Sushi',
        ratings: 3,
        reviews: '18 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Desi Katta',
        type: 'Indian',
        ratings: 2,
        reviews: '8 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Chings',
        type: 'Chinese',
        ratings: 5,
        reviews: '12 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Yamazaki',
        type: 'Japanese',
        ratings: 1,
        reviews: '20 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },{
        name: 'Shreesh',
        type: 'Italian',
        ratings: 3,
        reviews: '298 reviews',
        image: '',
        location: {
            latitude: '',
            longitude: ''
        }
    },
]