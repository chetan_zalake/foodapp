import React, { Component } from 'react';
import { FlatList, View, StyleSheet } from 'react-native';
import RestaurantsListViewCell from './RestaurantsListViewCell'
import {restaurantsData} from './RestaurantsData'
// import {LogData} from '../../../../Utils/LogUtils'
import { VIEW_BACKGROUND_COLOR } from '../../../../constants/colors';
export default class RestaurantsListView extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={restaurantsData}
                    style={styles.tableStyle}
                    renderItem=
                    {({ item, index }) => {
                        return (
                            <RestaurantsListViewCell item={item} index={index} restaurantSelected = {(index) => this.props.restaurantSelected(index)}/>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tableStyle: {
        backgroundColor: VIEW_BACKGROUND_COLOR
    }
})
