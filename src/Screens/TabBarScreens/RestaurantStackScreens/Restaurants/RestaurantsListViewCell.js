import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, Platform, TouchableOpacity } from 'react-native';
import { GRAY_COLOR, VIEW_BACKGROUND_COLOR, APP_GREEN, WHITE_COLOR } from '../../../../constants/colors';
import { SF_PRO_BOLD, SF_PRO_REGULAR } from '../../../../constants/fonts';

export default class RecipieListViewCell extends PureComponent {

    render() {

        let ratingView = [];

        for (let i = 0; i < 5; i++) {
            if (i > this.props.item.ratings)
                ratingView.push(
                    <Image key = {i.toString()} style={{ width: 22, height: 22, tintColor: APP_GREEN, opacity: 0.2 }} source={require('../../../../../assets/images/icons-circle/icons-circle.png')} />
                )
            else
            ratingView.push(
                <Image key = {i.toString()} style={{ width: 22, height: 22, tintColor: APP_GREEN }} source={require('../../../../../assets/images/icons-circle/icons-circle.png')} />
            )
        }
        return (
            <TouchableOpacity onPress = {() => this.props.restaurantSelected(this.props.index)} style={styles.cellStyle}>
                <View style={{ padding: 15, flex: 0.70, justifyContent: 'space-between' }}>
                    <View>
                        <Text allowFontScaling={false} style={{ fontSize: 17, fontFamily: SF_PRO_BOLD, color: 'black' }}>{this.props.item.name}</Text>
                        <Text allowFontScaling={false} style={{ fontSize: 13, fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR }}>{this.props.item.type}</Text>
                    </View>

                    <View style={{ marginTop: 10, flexDirection: 'row', alignItems: 'center' }}>
                        {ratingView}
                        <Text allowFontScaling={false} style={{ fontSize: 14, marginLeft: 10, fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR }}>{this.props.item.reviews}</Text>
                    </View>
                </View>
                <View style={{ flex: 0.30, borderRadius: 8, marginVertical: 15, marginRight: 15, overflow: 'hidden' }}>
                    <Image resizeMode={'center'} style={{ width: '100%', height: '100%' }} source={require('../../../../../assets/images/rest_1.jpg')} />
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    separator: { backgroundColor: VIEW_BACKGROUND_COLOR, height: 1, width: '100%' },
    cellStyle: {
        borderRadius: 8, marginHorizontal: 15, marginVertical: 7.5, flexDirection: 'row', borderColor: 'black', backgroundColor: WHITE_COLOR, height: 100,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.8)', 
                shadowRadius: 5, 
                shadowOpacity: 0.2,
            },
            android: {
              elevation: 2
            }
          }),
    }
});


//<Text allowFontScaling={false} style={{ fontSize: 17, padding: 10, fontFamily: SF_PRO_REGULAR, color: GRAY_COLOR }}>{this.props.index + 1}</Text>