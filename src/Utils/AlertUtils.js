import {
    Alert
} from 'react-native'
import { LogData } from './LogUtils';

export const showAlert = (title, message) => {
    Alert.alert(
        title,
        message,
        [
            {text: 'OK', onPress: () => LogData('OK Pressed')}
        ]
    )
}

export const showAlertWithOkButtonAction = (title, message, okPressed) => {
    Alert.alert(
        title,
        message,
        [
            {text: 'OK', onPress: () => okPressed()},
        ]
    );
}

export const showAlertWithOutDismissButton = (title, message) => {
    Alert.alert(
        title,
        message
    )
}