import {PixelRatio} from 'react-native'

export const textFontSize = (minFontSize, maxFontSize) => PixelRatio.getFontScale() > 1.0 ? Math.min(PixelRatio.getFontScale() * minFontSize, maxFontSize) : minFontSize