export const APP_GREEN = '#73c700';
export const WHITE_COLOR = '#FFFFFF';
export const INACTIVE_TINT_COLOR = '#999999';
export const GRAY_COLOR = '#999999';
export const VIEW_BACKGROUND_COLOR = '#f3f3f3';
