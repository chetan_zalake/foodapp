export const SHOW_AUTHORIZATION_SCREEN = 'SHOW_AUTHORIZATION_SCREEN'
export const SHOW_SHOP_SCREEN = 'SHOW_SHOP_SCREEN'
export const SHOW_BUTTON_INDICATIONS_SCREEN = 'SHOW_BUTTON_INDICATIONS_SCREEN'
export const REMINDER_TO_READ_NOTIFICATION = 'REMINDER_TO_READ_NOTIFICATION'
export const NEXT_LESSON_NOTIFICATION = 'NEXT_LESSON_NOTIFICATION'
