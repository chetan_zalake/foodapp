import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { APP_GREEN } from '../../constants/colors';
import { SF_PRO_REGULAR } from '../../constants/fonts';

const IconWithTitleView = (props) => {
    let ratingView = [];
    if (props.isRatings){
        for (let i = 0; i < 5; i++) {
            if (i > props.ratingsCount)
                ratingView.push(
                    <Image key = {i.toString()} style={{ width: 22, height: 22, tintColor: APP_GREEN, opacity: 0.2 }} source={props.image} />
                )
            else
            ratingView.push(
                <Image key = {i.toString()} style={{ width: 22, height: 22, tintColor: APP_GREEN }} source={props.image} />
            )
        }
    }
    return (
        <View style={{ padding: 10, alignItems: 'center', justifyContent: 'space-evenly' }}>
            {
                props.isRatings ? <View style = {{flexDirection: 'row'}}>{ratingView}</View>:  <Image style={{ width: 22, height: 22, tintColor: APP_GREEN }} source={props.image} />
            }
           
            <Text allowFontScaling={false} style={{ fontSize: 11, fontFamily: SF_PRO_REGULAR, color: APP_GREEN, marginTop: 5 }}>{props.title}</Text>
        </View>
    );
}

export default IconWithTitleView