import React, { Component } from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView, TouchableOpacity, Platform } from 'react-native'
import { LogData } from '../../Utils/LogUtils';

const deviceWidth = Dimensions.get('window').width

export default class MultipleImageView extends Component {

  numItems = this.props.images.length
  animVal = new Animated.Value(0)

  constructor(props) {
    super(props)

    this.state = {
      currentPage: 0
    }
  }

  changetheImage = (index) => {
    this.setState({
      currentPage: index
    }, () => {
        this.props.setCurrentPage(index)
        this.refs.scroll.scrollTo({ x: deviceWidth * index });
    });
  }

  render() {

    let imageArray = []
    this.props.images.forEach((image, i) => {
      const thisImage = (
        
        <Image key = {i.toString()} style={{ width: deviceWidth, height: 225 }} source={require('../../../assets/images/rest_1.jpg')} />
      )
      imageArray.push(thisImage)
    })

    return (
      <View
        style={[styles.container, { height: this.props.height }]}
      >
        <ScrollView
          onMomentumScrollEnd={event => { 
            LogData("onMomentumScrollEnd ",Math.round(event.nativeEvent.contentOffset.x/deviceWidth))

            this.setState({
              currentPage: Platform.OS === 'ios' ?  event.nativeEvent.contentOffset.x/deviceWidth : Math.round(event.nativeEvent.contentOffset.x/deviceWidth)
            }, () => {
                this.props.setCurrentPage(this.state.currentPage)
            })
            // LogData(event.nativeEvent.contentOffset.y, )
          }}
          scrollEnabled={true}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={10}
          pagingEnabled
          ref={'scroll'}
        >

          {imageArray}

        </ScrollView>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    top: 40,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
})
