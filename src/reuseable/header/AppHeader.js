import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, StatusBar,Platform } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import styles from "./styles";
import { SF_PRO_BOLD } from '../../constants/fonts';
import { APP_GREEN, WHITE_COLOR } from '../../constants/colors';

export default class AppHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        let leftBarButton = this.props.showLeftIcon ?
            <TouchableOpacity onPress={this.props.onLeftButtonPressed.bind(this)} style={{ marginLeft: 20, width: 40, height: '100%', justifyContent: 'center', }}>
                <Icon style={{}} name="md-arrow-back" size={25} color = {WHITE_COLOR} onPress={() => this.props.onLeftButtonPressed.bind(this)} />
            </TouchableOpacity>
            : null

        let rightBarButton = this.props.showRightIcon ?
            <TouchableOpacity onPress={() => this.props.rightNavigationButtonAction()} style={{ marginRight: 15, width: 40, height: '100%', justifyContent: 'center', }}>
                <Image resizeMode={'contain'} style={{ flex: 1, tintColor: WHITE_COLOR }} source={this.props.rightIconImage} />
            </TouchableOpacity>
            : null

        return (
            <View style={{ width: '100%', height: Platform.OS === 'ios' ? 140 : 140 - StatusBar.currentHeight, backgroundColor: APP_GREEN }}>
                <View style = {{height: 44, width: '100%', flexDirection: 'row', marginTop: Platform.OS === 'ios' ? 44 : 44 - StatusBar.currentHeight}}>
                    {
                        <View style={{ flex: 0.50, backgroundColor: "transparent" }}>
                            {this.props.showLeftIcon ? leftBarButton : null}
                        </View>
                    }

                    {
                        <View style={{ flex: 0.50, backgroundColor: "transparent", alignItems: 'flex-end' }}>
                            {this.props.showRightIcon ? rightBarButton : null}
                        </View>
                    }
                </View>
                <View style = {{flex: 1}}>
                    <Text allowFontScaling={false} style = {{fontSize: 34, fontFamily: SF_PRO_BOLD, marginLeft: 14, color: WHITE_COLOR}}>{this.props.title}</Text>
                </View>
            </View>
        )
    }
}
