import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, Button, StatusBar, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import styles from "./styles";
import { LogData } from '../../Utils/LogUtils'
import { SF_PRO_BOLD, SF_PRO_REGULAR } from '../../constants/fonts';
import { APP_GREEN, WHITE_COLOR } from '../../constants/colors';

export default class BasicAppHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const navIconsBothFalse = (this.props.showLeftIcon === false || this.props.showLeftIcon === undefined) && (this.props.showRightIcon === false || this.props.showRightIcon === undefined)
        let leftBarButton = this.props.showLeftIcon ?
            this.props.leftBarButtonImage !== undefined ?
                <TouchableOpacity onPress={() => this.props.onLeftButtonPressed()} style={{ marginLeft: 9, height: '100%', justifyContent: 'center', }}>
                    <Image style={{ width: 16, height: 10 }}
                        source={this.props.leftBarButtonImage} />
                </TouchableOpacity> :
                <TouchableOpacity onPress={() => this.props.onLeftButtonPressed()} style={{ marginLeft: 9, height: '100%', alignItems: 'center', flexDirection: 'row'}}>
                    <Icon style={{}} name="md-arrow-back" size={25} color="white" onPress={() => this.props.onLeftButtonPressed()} />
                    <Text allowFontScaling={false} style = {{fontSize: 17, fontFamily: SF_PRO_REGULAR, color: WHITE_COLOR, marginLeft: 3}}>Recipes</Text>
                </TouchableOpacity> : null

        let rightBarButton = this.props.showRightIcon ?
            <TouchableOpacity onPress={() => this.props.rightNavigationButtonAction()} style={{ marginRight: 15, width: 40, height: '100%', alignItems: 'center' }}>
                <Image resizeMode={'contain'} style={{ flex: 1, tintColor: WHITE_COLOR }} source={this.props.rightIconImage} />
            </TouchableOpacity>
            : null

        return (
            <View style={{ width: '100%', height: Platform.OS === 'ios' ? 88 : 88 - StatusBar.currentHeight, backgroundColor: APP_GREEN }}>
            <View style = {{height: 44, width: '100%', flexDirection: 'row', marginTop: Platform.OS === 'ios' ? 44 : 44 - StatusBar.currentHeight}}>
                {
                    <View style={{ flex: 0.25, backgroundColor: "transparent" }}>
                        {this.props.showLeftIcon ? leftBarButton : null}
                    </View>
                }
                    <View style = {{flex: navIconsBothFalse ? 1.0 : 0.5, alignContent: 'center', justifyContent: 'center'}}>
                        <Text allowFontScaling={false} style = {{fontSize: 17, fontFamily: SF_PRO_REGULAR, marginLeft: 14, color: WHITE_COLOR, textAlign: 'center'}}>{this.props.title}</Text>
                    </View>
                {
                    <View style={{ flex: 0.25, backgroundColor: "transparent", alignItems: 'flex-end' }}>
                        {this.props.showRightIcon ? rightBarButton : null}
                    </View>
                }
            </View>
        </View>
        )
    }
}
