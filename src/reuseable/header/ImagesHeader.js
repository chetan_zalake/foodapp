import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, StatusBar } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import styles from "./styles";
import { SF_PRO_BOLD, SF_PRO_REGULAR, SF_PRO_MEDIUM } from '../../constants/fonts';
import { APP_GREEN, WHITE_COLOR } from '../../constants/colors';
import PaginationDot from 'react-native-animated-pagination-dot'
import MultipleImageScrollView from '../MultipleImagesScrollView/MultipleImagesScrollView'
import { LogData } from '../../Utils/LogUtils';

export default class ImagesHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentPage :0,
            maxPage:10
        }
    }

    setCurrentPage = (index) => {
        LogData("currentPage: ", index)
        this.setState({
            currentPage: index
        })
    }
    render() {
        let leftBarButton = this.props.showLeftIcon ?
            this.props.leftBarButtonImage !== undefined ?
                <TouchableOpacity onPress={() => this.props.onLeftButtonPressed()} style={{ marginLeft: 9, height: '100%', justifyContent: 'center', }}>
                    <Image style={{ width: 16, height: 10 }}
                        source={this.props.leftBarButtonImage} />
                </TouchableOpacity> :
                <TouchableOpacity onPress={() => this.props.onLeftButtonPressed()} style={{ marginLeft: 9, height: '100%', alignItems: 'center', flexDirection: 'row'}}>
                    <Icon style={{}} name="md-arrow-back" size={25} color="white" onPress={() => this.props.onLeftButtonPressed()} />
                    <Text allowFontScaling={false} style = {{fontSize: 17, fontFamily: SF_PRO_REGULAR, color: WHITE_COLOR, marginLeft: 3}}>{this.props.leftButtonTitle}</Text>
                </TouchableOpacity> : null

        let rightBarButton = this.props.showRightIcon ?
            <TouchableOpacity onPress={() => this.props.rightNavigationButtonAction()} style={{ marginRight: 15, width: 40, height: '100%', alignItems: 'center' }}>
                <Image resizeMode={'contain'} style={{ flex: 1, tintColor: WHITE_COLOR }} source={this.props.rightIconImage} />
            </TouchableOpacity>
            : null

            const {currentPage, maxPage} = this.state;
        return (
            <View style={{ width: '100%', height: 225 }}>
                <MultipleImageScrollView images = {this.props.images} height = {225}
                    setCurrentPage = {(index) => this.setCurrentPage(index)}
                />
                <View style = {{height: 44, width: '100%', flexDirection: 'row', marginTop: 44, position: 'absolute'}}>
                    {
                        <View style={{ flex: 0.50, backgroundColor: "transparent" }}>
                            {this.props.showLeftIcon ? leftBarButton : null}
                        </View>
                    }

                    {
                        <View style={{ flex: 0.50, backgroundColor: "transparent", alignItems: 'flex-end' }}>
                            {this.props.showRightIcon ? rightBarButton : null}
                        </View>
                    }
                </View>
                <View style = {{bottom: 10, width: '100%', height: 80, position: 'absolute', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text    allowFontScaling={false} style = {{alignSelf: 'flex-end',fontSize: 34,fontFamily: SF_PRO_MEDIUM, marginLeft: 15, color: WHITE_COLOR, maxWidth: '70%'}}>{this.props.title}</Text>
                    <View style = {{ marginRight: 15, alignItems: 'flex-end', alignSelf: 'flex-end'}}>
                    <PaginationDot 
                        activeDotColor='white' 
                        curPage={currentPage} 
                        maxPage={maxPage}
                    />
                    </View>
                </View>
            </View>
        )
    }
}
