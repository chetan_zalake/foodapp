import { StyleSheet, Dimensions, Platform, StatusBar } from 'react-native'
import { SF_PRO_BOLD } from '../../constants/fonts';


export default {

    left_icon: {
        // fontSize: 30,
        // fontWeight: 'bold',
        alignSelf: "flex-start"
    },
    right_icon: {
        fontSize: 30,
        color: "black",
        fontWeight: 'bold',
    },
    title: {
        fontSize: 32,
        fontFamily: SF_PRO_BOLD,
        color: '#FFFFFF',
    },
    logo: {
        position: "absolute",
        left: Platform.OS === "android" ? 40 : 50,
        top: Platform.OS === "android" ? 35 : 60,
        width: 280,
        height: Platform.OS === 'android' ?  120 : 145 
    },
    text: {
        color: "#D8D8D8",
        bottom: 6,
        marginTop: 5
    },
    header_view: {
        flexDirection: "row",
        alignContent: "center",
        justifyContent: "center",
        height: Platform.OS === 'android' ?  120 : 145,
        width: '100%'
    },
    header_inner_view: {
        // marginTop:(Platform.OS === 'ios')?5:10,
        alignItems: "center",
        justifyContent: "center",
        height: Platform.OS === 'android' ?  120 : 145 
    },
    basic_header_inner_view: {
        marginTop: Platform.OS === 'android' ? 0 : 22,
        alignItems: "center",
        justifyContent: "center",
        height: Platform.OS === 'android' ?  75 : 100 
    },
    basic_header_view: {
        flexDirection: "row",
        alignContent: "center",
        justifyContent: "center",
        height: Platform.OS === 'android' ?  75 : 100,
        width: '100%'
    },
    basic_title: {
        fontSize: 18,
        fontFamily: SF_PRO_BOLD,
        color: '#FFFFFF'
    }
};